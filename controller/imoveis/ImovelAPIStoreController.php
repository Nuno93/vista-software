<?php
class ImovelAPIStore
{

    public function __construct()
    {
        $this->store();
    }

    private function store()
    {
        try {
            $categoria = $_POST['categoria'];
            $endereco = $_POST['endereco'];
            $numeroend = $_POST['numeroend'];
            $complemento = $_POST['complemento'];
            $bairro = $_POST['bairro'];
            $cidade = $_POST['cidade'];
            $uf = $_POST['uf'];
            $cep = $_POST['cep'];
            $situacao = $_POST['situacao'];
            $ocupacao = $_POST['ocupacao'];
            var_dump($ocupacao);
            die;
            $this->insereAPI($categoria, $endereco, $numeroend, $complemento, $bairro, $cidade, $uf, $cep, $situacao, $ocupacao);
            echo "<script>alert('Registro incluído com sucesso!');document.location='../../view/imoveis/index-imovel.php'</script>";
        } catch (Exception $e) {
            echo "<script>alert('Erro ao gravar registro! " . $e->getMessage() . "');history.back()</script>";
        }
    }

    private function insereAPI($categoria, $endereco, $numeroend, $complemento, $bairro, $cidade, $uf, $cep, $situacao, $ocupacao)
    {
        $postData = http_build_query(array('Categoria' => $categoria, 'Endereco' => $endereco, 'NumeroEnd' => $numeroend, 'Complemento' => $complemento, 'Bairro' => $bairro, 'Cidade' => $cidade, 'UF' => $uf, 'CEP' => $cep, 'Situacao' => $situacao, 'Ocupacao' => $ocupacao));
        $url = 'http://sandbox-rest.vistahost.com.br/imoveis/detalhes?key=c9fdd79584fb8d369a6a579af1a8f681';
        $headers = array('Accept: application/json');
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        $imoveis = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return json_decode($imoveis);
    }
}
