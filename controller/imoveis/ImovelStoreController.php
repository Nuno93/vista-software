<?php
require_once("../../model/Imovel/Imovel.php");
class ImovelStore
{

    private $imovel;

    public function __construct()
    {
        $this->imovel = new Imovel();
        $this->store();
    }

    private function store()
    {
        try {

            if ($_POST['id_proprietario'] == 0) {
                throw new Exception("Selecione um proprietário!");
            }
            $this->imovel->setEndereco($_POST['endereco']);
            $this->imovel->setIdProprietario($_POST['id_proprietario']);
            $result = $this->imovel->include();

            echo "<script>alert('Registro incluído com sucesso!');document.location='../../view/imoveis/index-imovel.php'</script>";
        } catch (Exception $e) {
            echo "<script>alert('Erro ao gravar registro! " . $e->getMessage() . "');history.back()</script>";
        }
    }
}
new ImovelStore();
