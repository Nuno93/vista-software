<?php
require_once("../../model/Imovel/databaseImovel.php");
class ImovelList
{

    private $lista;

    public function __construct()
    {
        $this->lista = new DatabaseImovel();
        $this->list();
    }

    private function list()
    {
        $row = $this->lista->getImovel();
        foreach ($row as $value) {
            echo "<tr>";
            echo "<th>" . $value['endereco'] . "</th>";
            echo "<td>" . $value['nome_proprietario'] . "</td>";
            echo "<td><a class='btn btn-outline-secondary' href='edit-imovel.php?id=" . $value['id'] . "'>Editar</i>";
            echo "<td></a><a class='btn btn-outline-danger' href='../../controller/imoveis/ImovelDeleteController.php?id=" . $value['id'] . "'>Excluir</a></td>";
            echo "</tr>";
        }
    }
}
