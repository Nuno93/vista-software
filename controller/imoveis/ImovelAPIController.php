<?php
ini_set('display_errors', 0);
error_reporting(0);
class ImovelAPI
{

    private $lista;

    public function __construct()
    {
        $this->list();
    }

    private function list()
    {
        $row = $this->consultaAPI();
        foreach ($row as $value) {
            echo "<tr>";
            echo "<th>" . $value->Codigo . "</th>";
            echo "<td>" . $value->Cidade . "</td>";
            echo "<td>" . $value->Categoria . "</td>";
            echo "</tr>";
        }
    }

    private function consultaAPI()
    {

        $url = 'http://sandbox-rest.vistahost.com.br/imoveis/listar?key=c9fdd79584fb8d369a6a579af1a8f681&showtotal=1&pesquisa={"fields":["Codigo","Categoria","Bairro","Cidade","ValorVenda","ValorLocacao"],"order":{"Bairro":"asc"},"paginacao":{"pagina":1,"quantidade":20}}';
        $headers = array('Accept: application/json');
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $imoveis = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return json_decode($imoveis);
    }
}
