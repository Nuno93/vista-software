<?php
require_once("../../model/Imovel/databaseImovel.php");

class ImovelEdit
{

    private $edit;
    private $endereco;
    private $id_proprietario;
    private $nome_proprietario;


    public function __construct($id)
    {
        $this->edit = new DatabaseImovel();
        $this->form($id);
    }
    private function form($id)
    {
        $row = $this->edit->searchImovel($id);
        $this->id               = $id;
        $this->endereco         = $row['endereco'];
        $this->id_proprietario  = $row['id_proprietario'];
        $this->nome_proprietario  = $row['nome_proprietario'];
    }
    public function editImovel($endereco, $id_proprietario, $id)
    {
        try {
            $this->edit->updateImovel($endereco, $id_proprietario, $id);
            echo "<script>alert('Registro editado com sucesso!');document.location='../../view/imoveis/index-imovel.php'</script>";
        } catch (Exception $e) {
            echo "<script>alert('Erro ao gravar registro! " . $e->getMessage() . "');history.back()</script>";
        }
    }
    public function getEndereco()
    {
        return $this->endereco;
    }
    public function getIdProprietario()
    {
        return $this->id_proprietario;
    }
    public function getNomeProprietario()
    {
        return $this->nome_proprietario;
    }

    public function getId()
    {
        return $this->id;
    }
}
$id = filter_input(INPUT_GET, 'id');
$edit = new ImovelEdit($id);
if (isset($_POST['submit'])) {
    $edit->editImovel($_POST['endereco'], $_POST['id_proprietario'], $_POST['id']);
}
