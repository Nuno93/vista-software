<?php
require_once("../../model/Imovel/databaseImovel.php");
class ImovelDelete
{
    private $delete;

    public function __construct($id)
    {
        $this->delete = new DatabaseImovel();
        try {
            $this->delete->deleteImovel($id);
            echo "<script>alert('Registro deletado com sucesso!');document.location='../../view/imoveis/index-imovel.php'</script>";
        } catch (Exception $e) {
            echo "<script>alert('Erro ao deletar registro! " . $e->getMessage() . "');history.back()</script>";
        }
    }
}
new ImovelDelete($_GET['id']);
