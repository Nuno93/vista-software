<?php
require_once("../../model/Repasse/databaseRepasse.php");
class RepasseList
{

    private $lista;
    private $id_contrato;

    public function __construct($id)
    {
        $this->id_contrato = $id;
        $this->lista = new DatabaseRepasse();
        $this->list();
    }

    private function list()
    {
        $row = $this->lista->getRepasse($this->id_contrato);
        foreach ($row as $value) {
            echo "<tr>";
            echo "<td>" . $value['id_contrato'] . "</td>";
            echo "<td>" . date('d/m/Y', strtotime($value['data_repasse'])) . "</td>";
            if ($value['realizado'] == 1) {
                echo "<td>Repasse Realizado!</td>";
            } else {
                echo "<td></a><a class='btn btn-outline-success' href='../../controller/repasses/RepasseEditController.php?id=" . $value['id'] . "&id_contrato=" . $value['id_contrato'] . "'>Realizar Repasse</a></td>";
            }
            echo "</tr>";
        }
    }
}
