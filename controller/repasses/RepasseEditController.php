<?php
require_once("../../model/Repasse/databaseRepasse.php");
class RepasseEdit
{
    private $edit;

    public function __construct($id, $id_contrato)
    {
        $this->edit = new DatabaseRepasse();
        try {
            $this->edit->updateRepasse($id);
            echo "<script>alert('Repasse feito com Sucesso!');document.location='../../view/contratos/repasses-contrato.php?id=" . $id_contrato . "'</script>";
        } catch (Exception $e) {
            echo "<script>alert('Erro ao realizar repasse! " . $e->getMessage() . "');history.back()</script>";
        }
    }
}
new RepasseEdit($_GET['id'], $_GET['id_contrato']);
