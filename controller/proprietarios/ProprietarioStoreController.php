<?php
require_once("../../model/Proprietario/Proprietario.php");
class ProprietarioStore
{

    private $proprietario;

    public function __construct()
    {
        $this->proprietario = new Proprietario();
        $this->store();
    }

    private function store()
    {
        try {

            if ((strpos($_POST['email'], '@') === false) || (strpos($_POST['email'], '.com') === false)) {
                throw new Exception("Insira um e-mail válido!");
            }

            $this->proprietario->setNome($_POST['nome']);
            $this->proprietario->setEmail($_POST['email']);
            $this->proprietario->setTelefone($_POST['telefone']);
            $this->proprietario->setDiaRepasse($_POST['dia_repasse']);
            $result = $this->proprietario->include();
            echo "<script>alert('Registro incluído com sucesso!');document.location='../../view/proprietarios/index-proprietario.php'</script>";
        } catch (Exception $e) {
            echo "<script>alert('Erro ao gravar registro! " . $e->getMessage() . "');history.back()</script>";
        }
    }
}
new ProprietarioStore();
