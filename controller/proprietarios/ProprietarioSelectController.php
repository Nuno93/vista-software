<?php
require_once("../../model/Proprietario/databaseProprietario.php");
class ProprietarioSelect
{

    private $lista;

    public function __construct()
    {
        $this->lista = new DatabaseProprietario();
        $this->list();
    }

    private function list()
    {
        $row = $this->lista->getProprietario();
        foreach ($row as $value) {
            echo "<option value='" . $value['id'] . "'>" . $value['nome'] . "</option>";
        }
    }
}
