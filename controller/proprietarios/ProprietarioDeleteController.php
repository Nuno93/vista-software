<?php
require_once("../../model/Proprietario/databaseProprietario.php");
class ProprietarioDelete
{
    private $delete;

    public function __construct($id)
    {
        $this->delete = new DatabaseProprietario();
        try {
            $this->delete->deleteProprietario($id);
            echo "<script>alert('Registro deletado com sucesso!');document.location='../../view/proprietarios/index-proprietario.php'</script>";
        } catch (Exception $e) {
            echo "<script>alert('Erro ao deletar registro! " . $e->getMessage() . "');history.back()</script>";
        }
    }
}
new ProprietarioDelete($_GET['id']);
