<?php
require_once("../../model/Proprietario/databaseProprietario.php");

class ProprietarioEdit
{

    private $edit;
    private $nome;
    private $email;
    private $telefone;
    private $dia_repasse;
    private $id;


    public function __construct($id)
    {
        $this->edit = new DatabaseProprietario();
        $this->form($id);
    }
    private function form($id)
    {
        $row = $this->edit->searchProprietario($id);
        $this->id           = $id;
        $this->nome         = $row['nome'];
        $this->email        = $row['email'];
        $this->telefone     = $row['telefone'];
        $this->dia_repasse  = $row['dia_repasse'];
    }
    public function editProprietario($nome, $email, $telefone, $dia_repasse, $id)
    {
        try {
            $this->edit->updateProprietario($nome, $email, $telefone, $dia_repasse, $id);
            echo "<script>alert('Registro editado com sucesso!');document.location='../../view/proprietarios/index-proprietario.php'</script>";
        } catch (Exception $e) {
            echo "<script>alert('Erro ao gravar registro! " . $e->getMessage() . "');history.back()</script>";
        }
    }
    public function getNome()
    {
        return $this->nome;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getTelefone()
    {
        return $this->telefone;
    }
    public function getDiaRepasse()
    {
        return $this->dia_repasse;
    }
    public function getId()
    {
        return $this->id;
    }
}
$id = filter_input(INPUT_GET, 'id');
$edit = new ProprietarioEdit($id);
if (isset($_POST['submit'])) {
    $edit->editProprietario($_POST['nome'], $_POST['email'], $_POST['telefone'], $_POST['dia_repasse'], $_POST['id']);
}
