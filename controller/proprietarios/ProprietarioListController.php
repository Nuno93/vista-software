<?php
require_once("../../model/Proprietario/databaseProprietario.php");
class ProprietarioList
{

    private $lista;

    public function __construct()
    {
        $this->lista = new DatabaseProprietario();
        $this->list();
    }

    private function list()
    {
        $row = $this->lista->getProprietario();
        foreach ($row as $value) {
            echo "<tr>";
            echo "<th>" . $value['nome'] . "</th>";
            echo "<td>" . $value['email'] . "</td>";
            echo "<td>" . $value['telefone'] . "</td>";
            echo "<td>" . $value['dia_repasse'] . "</td>";
            echo "<td><a class='btn btn-outline-secondary' href='edit-proprietario.php?id=" . $value['id'] . "'>Editar</a>";
            echo "<td><a class='btn btn-outline-danger' href='../../controller/proprietarios/ProprietarioDeleteController.php?id=" . $value['id'] . "'>Excluir</a></td>";
            echo "</tr>";
        }
    }
}
