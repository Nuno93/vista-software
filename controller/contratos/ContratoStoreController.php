<?php
require_once("../../model/Contrato/Contrato.php");
require_once("../../model/Mensalidade/Mensalidade.php");
require_once("../../model/Repasse/Repasse.php");
require_once("../../model/Imovel/databaseImovel.php");
require_once("../../model/Proprietario/databaseProprietario.php");
class ContratoStore
{

    private $contrato;

    public function __construct()
    {
        $this->contrato = new Contrato();
        $this->mensalidade = new Mensalidade();
        $this->repasse = new Repasse();
        $this->imovel = new DatabaseImovel();
        $this->proprietario = new DatabaseProprietario();
        $this->store();
    }

    private function store()
    {
        try {
            $imovel = $this->imovel->searchImovel($_POST['id_imovel']);
            $id_proprietario = $imovel['id_proprietario'];

            $proprietario = $this->proprietario->searchProprietario($id_proprietario);
            $dia_repasse = $proprietario['dia_repasse'];

            $data_inicial = $_POST['data_inicio'];
            $data_final = $_POST['data_fim'];

            if (new DateTime($data_inicial) > new DateTime($data_final)) {
                throw new Exception("Período de datas incorreto. Data Inicial deve ser anterior a Data Final!");
            }

            if (new DateTime($data_inicial) < new DateTime()) {
                throw new Exception("Data Inicial deve ser posterior ao dia de hoje!");
            }

            if ($_POST['id_imovel'] == 0) {
                throw new Exception("Selecione um imóvel!");
            }

            if ($_POST['id_cliente'] == 0) {
                throw new Exception("Selecione um cliente!");
            }

            $this->contrato->setIdCliente($_POST['id_cliente']);
            $this->contrato->setIdProprietario($id_proprietario);
            $this->contrato->setIdImovel($_POST['id_imovel']);
            $this->contrato->setDataInicio(date('Y-m-d', strtotime($_POST['data_inicio'])));
            $this->contrato->setDataFim(date('Y-m-d', strtotime($_POST['data_fim'])));
            $this->contrato->setTaxaAdm($_POST['taxa_adm']);
            $this->contrato->setValorAluguel($_POST['valor_aluguel']);
            $this->contrato->setValorCondominio($_POST['valor_condominio']);
            $this->contrato->setValorIptu($_POST['valor_iptu']);
            $resultContrato = $this->contrato->include();

            $data1 = array_combine(['y', 'm', 'd'], explode('-', $_POST['data_inicio']));
            $data2 = array_combine(['y', 'm', 'd'], explode('-', $_POST['data_fim']));
            $quantidade_meses = abs($data1['m'] - $data2['m']) + (($data2['y'] - $data1['y']) * 12);
            $data_vencimento = explode("-", $data_inicial);
            list($ano, $mes, $dia) = $data_vencimento;

            for ($i = 1; $i <= $quantidade_meses; $i++) {
                $mes++;
                if ($mes > 12) {
                    $mes = 1;
                    $ano++;
                }
                $data_vencimento = "$ano-$mes-1";
                $data_repasse = "$ano-$mes-$dia_repasse";
                if ($i === 1) {
                    $vencimento = new DateTime($data_vencimento);
                    $inicial = new DateTime($data_inicial);
                    $dias = $vencimento->diff($inicial);
                    $valor_mensalidade = $_POST['valor_aluguel'] / 30 * $dias->d;
                    $valor_mensalidade = round($valor_mensalidade, 2);
                } else {
                    $valor_mensalidade = $_POST['valor_aluguel'];
                }
                $this->mensalidade->setIdContrato($resultContrato);
                $this->mensalidade->setPago(0);
                $this->mensalidade->setDataVencimento(date('Y-m-d', strtotime($data_vencimento)));
                $this->mensalidade->setValorMensalidade($valor_mensalidade);
                $resultMensalidade = $this->mensalidade->include();

                $this->repasse->setIdContrato($resultContrato);
                $this->repasse->setRealizado(0);
                $this->repasse->setDataRepasse($data_repasse);
                $resultRepasse = $this->repasse->include();
            }
            echo "<script>alert('Registro incluído com sucesso!');document.location='../../view/contratos/index-contrato.php'</script>";
        } catch (Exception $e) {
            echo "<script>alert('" . $e->getMessage() . "');history.back()</script>";
        }
    }
}
new ContratoStore();
