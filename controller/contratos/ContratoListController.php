<?php
require_once("../../model/Contrato/databaseContrato.php");
class ContratoList
{

    private $lista;

    public function __construct()
    {
        $this->lista = new DatabaseContrato();
        $this->list();
    }

    private function list()
    {
        $row = $this->lista->getContrato();
        foreach ($row as $value) {
            echo "<tr>";
            echo "<td>" . $value['nome_cliente'] . "</td>";
            echo "<td>" . $value['nome_proprietario'] . "</td>";
            echo "<td>" . $value['valor_aluguel'] . "</td>";
            echo "<td>" . date('d/m/Y', strtotime($value['data_inicio'])) . "</td>";
            echo "<td>" . date('d/m/Y', strtotime($value['data_fim'])) . "</td>";
            echo "<td><a class='btn btn-outline-success' href='mensalidades-contrato.php?id=" . $value['id'] . "'>Acessar</i>";
            echo "<td><a class='btn btn-outline-primary' href='repasses-contrato.php?id=" . $value['id'] . "'>Acessar</i>";
            echo "<td></a><a class='btn btn-outline-danger' href='../../controller/contratos/ContratoDeleteController.php?id=" . $value['id'] . "'>Excluir</a></td>";
            echo "</tr>";
        }
    }
}
