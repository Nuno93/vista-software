<?php
require_once("../../model/Contrato/databaseContrato.php");

class ContratoEdit
{

    private $edit;
    private $id_cliente;
    private $id_proprietario;
    private $id_imovel;
    private $data_inicio;
    private $data_fim;
    private $taxa_adm;
    private $valor_aluguel;
    private $valor_condominio;
    private $valor_iptu;

    public function __construct($id)
    {
        $this->edit = new DatabaseContrato();
        $this->form($id);
    }
    private function form($id)
    {
        $row = $this->edit->searchContrato($id);
        $this->id               = $id;
        $this->id_cliente       = $row['id_cliente'];
        $this->id_proprietario  = $row['id_proprietario'];
        $this->id_imovel        = $row['id_imovel'];
        $this->data_inicio      = $row['data_inicio'];
        $this->data_fim         = $row['data_fim'];
        $this->taxa_adm         = $row['taxa_adm'];
        $this->valor_aluguel    = $row['valor_aluguel'];
        $this->valor_condominio = $row['valor_condominio'];
        $this->valor_iptu       = $row['valor_iptu'];
    }
    public function editContrato(
        $id_cliente,
        $id_proprietario,
        $id_imovel,
        $data_inicio,
        $data_fim,
        $taxa_adm,
        $valor_aluguel,
        $valor_condominio,
        $valor_iptu,
        $id
    ) {
        try {
            $this->edit->updateContrato(
                $id_cliente,
                $id_proprietario,
                $id_imovel,
                $data_inicio,
                $data_fim,
                $taxa_adm,
                $valor_aluguel,
                $valor_condominio,
                $valor_iptu,
                $id
            );
            echo "<script>alert('Registro editado com sucesso!');document.location='../../view/contratos/index-contrato.php'</script>";
        } catch (Exception $e) {
            echo "<script>alert('Erro ao gravar registro!  " . $e->getMessage() . "');history.back()</script>";
        }
    }
    public function getIdCliente()
    {
        return $this->id_cliente;
    }
    public function getIdProprietario()
    {
        return $this->id_proprietario;
    }
    public function getIdImovel()
    {
        return $this->id_imovel;
    }
    public function getDataInicio()
    {
        return $this->data_inicio;
    }

    public function getDataFim()
    {
        return $this->data_fim;
    }

    public function getTaxaAdm()
    {
        return $this->taxa_adm;
    }

    public function getValorAluguel()
    {
        return $this->valor_aluguel;
    }

    public function getValorCondominio()
    {
        return $this->valor_condominio;
    }

    public function getValorIptu()
    {
        return $this->valor_iptu;
    }

    public function getId()
    {
        return $this->id;
    }
}
$id = filter_input(INPUT_GET, 'id');
$edit = new ContratoEdit($id);
if (isset($_POST['submit'])) {
    $edit->editContrato(
        $_POST['id_cliente'],
        $_POST['id_proprietario'],
        $_POST['id_imovel'],
        $_POST['data_inicio'],
        $_POST['data_fim'],
        $_POST['taxa_adm'],
        $_POST['valor_aluguel'],
        $_POST['valor_condominio'],
        $_POST['valor_iptu'],
        $_POST['id']
    );
}
