<?php
require_once("../../model/Contrato/databaseContrato.php");
class ContratoDelete
{
    private $delete;

    public function __construct($id)
    {
        try {
            $this->delete = new DatabaseContrato();
            $this->delete->deleteContrato($id);
            echo "<script>alert('Registro deletado com sucesso!');document.location='../../view/contratos/index-contrato.php'</script>";
        } catch (Exception $e) {
            echo "<script>alert('Erro ao deletar registro! " . $e->getMessage() . "');history.back()</script>";
        }
    }
}
new ContratoDelete($_GET['id']);
