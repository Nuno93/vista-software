<?php
require_once("../../model/Cliente/databaseCliente.php");
class ClienteList
{

    private $lista;

    public function __construct()
    {
        $this->lista = new DatabaseCliente();
        $this->list();
    }

    private function list()
    {
        $row = $this->lista->getCliente();
        foreach ($row as $value) {
            echo "<tr>";
            echo "<th>" . $value['nome'] . "</th>";
            echo "<td>" . $value['email'] . "</td>";
            echo "<td>" . $value['telefone'] . "</td>";
            echo "<td><a class='btn btn-outline-secondary' href='edit-cliente.php?id=" . $value['id'] . "'>Editar</i></a>";
            echo "<td><a class='btn btn-outline-danger' href='../../controller/clientes/ClienteDeleteController.php?id=" . $value['id'] . "'>Excluir</a></td>";
            echo "</tr>";
        }
    }
}
