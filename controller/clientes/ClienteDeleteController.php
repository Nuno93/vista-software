<?php
require_once("../../model/Cliente/databaseCliente.php");
class ClienteDelete
{
    private $delete;

    public function __construct($id)
    {
        try {
            $this->delete = new DatabaseCliente();
            $this->delete->deleteCliente($id);
            echo "<script>alert('Registro deletado com sucesso!');document.location='../../view/clientes/index-cliente.php'</script>";
        } catch (Exception $e) {
            echo "<script>alert('Erro ao deletar registro! " . $e->getMessage() . "');history.back()</script>";
        }
    }
}
new ClienteDelete($_GET['id']);
