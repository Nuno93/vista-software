<?php
require_once("../../model/Cliente/databaseCliente.php");

class ClienteEdit
{

    private $edit;
    private $nome;
    private $email;
    private $telefone;


    public function __construct($id)
    {
        $this->edit = new DatabaseCliente();
        $this->form($id);
    }
    private function form($id)
    {
        $row = $this->edit->searchCliente($id);
        $this->id           = $id;
        $this->nome         = $row['nome'];
        $this->email        = $row['email'];
        $this->telefone     = $row['telefone'];
    }
    public function editCliente($nome, $email, $telefone, $id)
    {
        try {
            $this->edit->updateCliente($nome, $email, $telefone, $id);
            echo "<script>alert('Registro editado com sucesso!');document.location='../../view/clientes/index-cliente.php'</script>";
        } catch (Exception $e) {
            echo "<script>alert('Erro ao gravar registro! " . $e->getMessage() . "');history.back()</script>";
        }
    }
    public function getNome()
    {
        return $this->nome;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getTelefone()
    {
        return $this->telefone;
    }
    public function getId()
    {
        return $this->id;
    }
}
$id = filter_input(INPUT_GET, 'id');
$edit = new ClienteEdit($id);
if (isset($_POST['submit'])) {
    $edit->editCliente($_POST['nome'], $_POST['email'], $_POST['telefone'], $_POST['id']);
}
