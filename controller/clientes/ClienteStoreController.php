<?php
require_once("../../model/Cliente/Cliente.php");
class ClienteStore
{

    private $cliente;

    public function __construct()
    {
        $this->cliente = new Cliente();
        $this->store();
    }

    private function store()
    {
        try {

            if ((strpos($_POST['email'], '@') === false) || (strpos($_POST['email'], '.com') === false)) {
                throw new Exception("Insira um e-mail válido!");
            }

            $this->cliente->setNome($_POST['nome']);
            $this->cliente->setEmail($_POST['email']);
            $this->cliente->setTelefone($_POST['telefone']);
            $result = $this->cliente->include();

            echo "<script>alert('Registro incluído com sucesso!');document.location='../../view/clientes/index-cliente.php'</script>";
        } catch (Exception $e) {
            echo "<script>alert('Erro ao gravar registro! " . $e->getMessage() . "');history.back()</script>";
        }
    }
}
new ClienteStore();
