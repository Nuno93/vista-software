<?php
require_once("../../model/Cliente/databaseCliente.php");
class ClienteSelect
{

    private $lista;

    public function __construct()
    {
        $this->lista = new DatabaseCliente();
        $this->list();
    }

    private function list()
    {
        $row = $this->lista->getCliente();
        foreach ($row as $value) {
            echo "<option value='" . $value['id'] . "'>" . $value['nome'] . "</option>";
        }
    }
}
