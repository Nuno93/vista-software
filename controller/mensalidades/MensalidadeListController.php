<?php
require_once("../../model/Mensalidade/databaseMensalidade.php");
class MensalidadeList
{

    private $lista;
    private $id_contrato;

    public function __construct($id)
    {
        $this->id_contrato = $id;
        $this->lista = new DatabaseMensalidade();
        $this->list();
    }

    private function list()
    {
        $row = $this->lista->getMensalidade($this->id_contrato);
        foreach ($row as $value) {
            echo "<tr>";
            echo "<td>" . $value['id_contrato'] . "</td>";
            echo "<td>" . $value['valor_mensalidade'] . "</td>";
            echo "<td>" . date('d/m/Y', strtotime($value['data_vencimento'])) . "</td>";
            if ($value['pago'] == 1) {
                echo "<td>Mensalidade Paga!</td>";
            } else {
                echo "<td></a><a class='btn btn-outline-success' href='../../controller/mensalidades/MensalidadeEditController.php?id=" . $value['id'] . "&id_contrato=" . $value['id_contrato'] . "'>Sinalizar como Pago</a></td>";
            }
            echo "</tr>";
        }
    }
}
