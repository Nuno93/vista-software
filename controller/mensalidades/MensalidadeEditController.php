<?php
require_once("../../model/Mensalidade/databaseMensalidade.php");
class MensalidadeEdit
{
    private $edit;

    public function __construct($id, $id_contrato)
    {
        $this->edit = new DatabaseMensalidade();
        try {
            $this->edit->updateMensalidade($id);
            echo "<script>alert('Mensalidade Paga com Sucesso!');document.location='../../view/contratos/mensalidades-contrato.php?id=" . $id_contrato . "'</script>";
        } catch (Exception $e) {
            echo "<script>alert('Erro ao realizar repasse! " . $e->getMessage() . "');history.back()</script>";
        }
    }
}
new MensalidadeEdit($_GET['id'], $_GET['id_contrato']);
