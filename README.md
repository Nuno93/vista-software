# Teste de Desenvolvimento - Vista Software

Nome: Jair Alberto Krummenauer Junior
Versão: 0.1 - Teste

### Requisitos:

1. Importar dump do banco de dados incluído na aplicação.
2. Nome do banco de dados: vista_soft.
3. Aplicação criada e executada através de ferramenta para servidor de aplicações (WAMP ou XAMP, por exemplo)


### Estrutura da Aplicação:

A Aplicação foi estruturada em um padrão MVC básico.

1. init.php - Arquivo que define as variáveis de conexão com banco de dados e constantes da aplicação.
2. index.php - Página inicial da aplicação.
3. controller - Pasta onde ficam os controladores da aplicação, para inserir, editar ou listar dados.
4. model - Pasta onde ficam as models da aplicação, bem como os comandos SQL.
5. view - Pasta onde ficam as views da aplicação, com as páginas, os formulários, estilos e etc.

### Detalhes:

1. Estilo(CSS) feito pelo link do Bootstrap mimificado.
2. Cada classe dos controladores, com suas respectivas ações, foram separados em arquivos diferentes, para evitar arquivos extensos e facilitar o acesso.

### Dificuldades:

1. Funções de script das HTML's para validação não estão funcionando corretamente, porém não deu tempo para revisá-las. Porém, foram feitos tratamentos para inserir os dados no back-end.
2. Foram feitos 2 etapas de Cadastros e Listagem dos Imóveis: Pela API e Localmente, salvando na aplicação. Fiz isso para ligar o imóvel com um proprietário, cadastrado pelo sistema e poder gerar os contratos. 
3. Para entregar a aplicação no prazo, o store para a API não foi totalmente finalizado.
4. O código necessita de algumas melhorias para padronização de projetos e também melhorias de funcionalidades na interface, na estética e na responsividade.