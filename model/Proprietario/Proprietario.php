<?php
require_once("databaseProprietario.php");

class Proprietario extends DatabaseProprietario
{

    private $nome;
    private $email;
    private $telefone;
    private $dia_repasse;

    //Metodos Set
    public function setNome($string)
    {
        $this->nome = $string;
    }
    public function setEmail($string)
    {
        $this->email = $string;
    }
    public function setTelefone($string)
    {
        $this->telefone = $string;
    }
    public function setDiaRepasse($date)
    {
        $this->dia_repasse = $date;
    }
    //Metodos Get
    public function getNome()
    {
        return $this->nome;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getTelefone()
    {
        return $this->telefone;
    }
    public function getDiaRepasse()
    {
        return $this->dia_repasse;
    }

    public function include()
    {
        return $this->setProprietario($this->getNome(), $this->getEmail(), $this->getTelefone(), $this->getDiaRepasse());
    }
}
