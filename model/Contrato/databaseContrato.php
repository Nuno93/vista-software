<?php

require_once("../../init.php");
class DatabaseContrato
{

    public function setContrato(
        $id_cliente,
        $id_proprietario,
        $id_imovel,
        $data_inicio,
        $data_fim,
        $taxa_adm,
        $valor_aluguel,
        $valor_condominio,
        $valor_iptu
    ) {
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        if (!$conn) {
            die("Falha ao conectar no banco de dados: " . mysqli_connect_error());
        }
        $sql = "INSERT INTO contratos (id_cliente, 
                                       id_proprietario, 
                                       id_imovel,
                                       data_inicio, 
                                       data_fim, 
                                       taxa_adm, 
                                       valor_aluguel, 
                                       valor_condominio, 
                                       valor_iptu) 
                VALUES ('$id_cliente',
                        '$id_proprietario', 
                        '$id_imovel',
                        '$data_inicio', 
                        '$data_fim', 
                        '$taxa_adm',
                        '$valor_aluguel',
                        '$valor_condominio', 
                        '$valor_iptu')";

        if (mysqli_query($conn, $sql)) {
            return mysqli_insert_id($conn);
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            die;
            return false;
        }
    }

    public function getContrato()
    {
        $array = [];
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $sql = "SELECT c.id AS id, 
                       c.id_proprietario AS id_proprietario, 
                       c.id_cliente AS id_cliente, 
                       c.id_imovel AS id_imovel, 
                       c.data_inicio AS data_inicio,
                       c.data_fim AS data_fim,
                       c.taxa_adm AS taxa_adm, 
                       c.valor_aluguel AS valor_aluguel, 
                       c.valor_condominio AS valor_condominio, 
                       c.valor_iptu AS valor_iptu, 
                       a.nome AS nome_cliente, 
                       p.nome AS nome_proprietario
                FROM contratos c, proprietarios p, clientes a, imoveis i 
                WHERE c.id_proprietario = p.id 
                  AND c.id_cliente = a.id 
                  AND c.id_imovel = i.id";
        $contratos = mysqli_query($conn, $sql);
        while ($row = $contratos->fetch_array(MYSQLI_ASSOC)) {
            $array[] = $row;
        }
        return $array;
    }

    public function deleteContrato($id)
    {
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $sql = "DELETE FROM contratos WHERE id = '$id'";
        if (mysqli_query($conn, $sql)) {
            return true;
        } else {
            return false;
        }
    }
    public function searchContrato($id)
    {
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $sql = "SELECT c.id AS id,
                       c.id_proprietario AS id_proprietario,
                       c.id_cliente AS id_cliente,  
                       c.id_imovel AS id_imovel,
                       c.data_inicio AS data_inicio,
                       c.data_fim AS data_fim,
                       c.taxa_adm AS taxa_adm,
                       c.valor_aluguel AS valor_aluguel, 
                       c.valor_condominio AS valor_condominio, 
                       c.valor_iptu AS valor_iptu,
                       p.nome as nome_cliente,
                       p.nome as nome_proprietario 
                FROM contratos c, proprietarios p, clientes l 
                WHERE c.id_proprietario = p.id 
                AND c.id_cliente = i.id 
                AND c.id = '$id'";
        $contrato = mysqli_query($conn, $sql);
        if (!$contrato) {

            return false;
        } else {

            return $contrato->fetch_array(MYSQLI_ASSOC);
        }
    }
    public function updateContrato(
        $id_cliente,
        $id_proprietario,
        $id_imovel,
        $data_inicio,
        $data_fim,
        $taxa_adm,
        $valor_aluguel,
        $valor_condominio,
        $valor_iptu,
        $id
    ) {
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $sql = "UPDATE contratos 
                SET id_cliente      = '$id_cliente', 
                    id_proprietario = '$id_proprietario' 
                    id_imovel       = '$id_imovel' 
                    data_inicio     = '$data_inicio' 
                    data_fim        = '$data_fim' 
                    taxa_adm        = '$taxa_adm' 
                    valor_aluguel   = '$valor_aluguel' 
                    valor_condominio= '$valor_condominio' 
                    valor_iptu      = '$valor_iptu' 
                WHERE id = '$id'";
        if (mysqli_query($conn, $sql)) {
            return true;
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            die;
            return false;
        }
    }
}
