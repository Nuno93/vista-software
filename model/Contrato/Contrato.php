<?php
require_once("databaseContrato.php");

class Contrato extends DatabaseContrato
{

    private $id_cliente;
    private $id_proprietario;
    private $id_imovel;
    private $data_inicio;
    private $data_fim;
    private $taxa_adm;
    private $valor_aluguel;
    private $valor_condominio;
    private $valor_iptu;


    //Metodos Set
    public function setIdCliente($id)
    {
        $this->id_cliente = $id;
    }
    public function setIdProprietario($id)
    {
        $this->id_proprietario = $id;
    }
    public function setIdImovel($id)
    {
        $this->id_imovel = $id;
    }
    public function setDataInicio($data)
    {
        $this->data_inicio = $data;
    }
    public function setDataFim($data)
    {
        $this->data_fim = $data;
    }
    public function setTaxaAdm($valor)
    {
        $this->taxa_adm = $valor;
    }
    public function setValorAluguel($valor)
    {
        $this->valor_aluguel = $valor;
    }
    public function setValorCondominio($valor)
    {
        $this->valor_condominio = $valor;
    }
    public function setValorIptu($valor)
    {
        $this->valor_iptu = $valor;
    }
    //Metodos Get
    public function getIdCliente()
    {
        return $this->id_cliente;
    }
    public function getIdProprietario()
    {
        return $this->id_proprietario;
    }
    public function getIdImovel()
    {
        return $this->id_imovel;
    }
    public function getDataInicio()
    {
        return $this->data_inicio;
    }
    public function getDataFim()
    {
        return $this->data_fim;
    }
    public function getTaxaAdm()
    {
        return $this->taxa_adm;
    }
    public function getValorAluguel()
    {
        return $this->valor_aluguel;
    }
    public function getValorCondominio()
    {
        return $this->valor_condominio;
    }
    public function getValorIptu()
    {
        return $this->valor_iptu;
    }

    public function include()
    {
        return $this->setContrato(
            $this->getIdCliente(),
            $this->getIdProprietario(),
            $this->getIdImovel(),
            $this->getDataInicio(),
            $this->getDataFim(),
            $this->getTaxaAdm(),
            $this->getValorAluguel(),
            $this->getValorCondominio(),
            $this->getValorIptu()
        );
    }
}
