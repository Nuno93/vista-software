<?php
require_once("databaseMensalidade.php");

class Mensalidade extends DatabaseMensalidade
{

    private $id_contrato;
    private $data_vencimento;
    private $pago;
    private $valor_mensalidade;

    //Metodos Set
    public function setIdContrato($id)
    {
        $this->id_contrato = $id;
    }
    public function setPago($bool)
    {
        $this->pago = $bool;
    }
    public function setDataVencimento($data)
    {
        $this->data_vencimento = $data;
    }
    public function setValorMensalidade($valor)
    {
        $this->valor_mensalidade = $valor;
    }
    //Metodos Get
    public function getIdContrato()
    {
        return $this->id_contrato;
    }
    public function getPago()
    {
        return $this->pago;
    }
    public function getDataVencimento()
    {
        return $this->data_vencimento;
    }
    public function getValorMensalidade()
    {
        return $this->valor_mensalidade;
    }

    public function include()
    {
        return $this->setMensalidade($this->getIdContrato(), $this->getPago(), $this->getDataVencimento(), $this->getValorMensalidade());
    }
}
