<?php
require_once("databaseImovel.php");

class Imovel extends DatabaseImovel
{

    private $endereco;
    private $id_proprietario;

    //Metodos Set
    public function setEndereco($string)
    {
        $this->endereco = $string;
    }
    public function setIdProprietario($id)
    {
        $this->id_proprietario = $id;
    }
    //Metodos Get
    public function getEndereco()
    {
        return $this->endereco;
    }
    public function getIdProprietario()
    {
        return $this->id_proprietario;
    }

    public function include()
    {
        return $this->setImovel($this->getEndereco(), $this->getIdProprietario());
    }
}
