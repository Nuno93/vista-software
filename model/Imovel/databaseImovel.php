<?php

require_once("../../init.php");
class DatabaseImovel
{

    public function setImovel($endereco, $id_proprietario)
    {
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        if (!$conn) {
            die("Falha ao conectar no banco de dados: " . mysqli_connect_error());
        }
        $sql = "INSERT INTO imoveis (endereco, id_proprietario) VALUES ('$endereco','$id_proprietario')";
        if (mysqli_query($conn, $sql)) {
            return true;
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            die;
            return false;
        }
    }

    public function getImovel()
    {
        $array = [];
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $sql = "SELECT i.id AS id, i.endereco AS endereco, p.id AS id_proprietario, p.nome as nome_proprietario FROM imoveis i, proprietarios p WHERE i.id_proprietario = p.id";
        $imoveis = mysqli_query($conn, $sql);
        while ($row = $imoveis->fetch_array(MYSQLI_ASSOC)) {
            $array[] = $row;
        }
        return $array;
    }

    public function deleteImovel($id)
    {
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $sql = "DELETE FROM imoveis WHERE id = '$id'";
        if (mysqli_query($conn, $sql)) {
            return true;
        } else {
            return false;
        }
    }
    public function searchImovel($id)
    {
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $sql = "SELECT i.id AS id, i.endereco AS endereco, p.id AS id_proprietario, p.nome as nome_proprietario FROM imoveis i, proprietarios p WHERE i.id_proprietario = p.id AND i.id='$id'";
        $imovel = mysqli_query($conn, $sql);
        if (!$imovel) {

            return false;
        } else {

            return $imovel->fetch_array(MYSQLI_ASSOC);
        }
    }
    public function updateImovel($endereco, $id_proprietario, $id)
    {
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $sql = "UPDATE imoveis SET endereco = '$endereco', id_proprietario = '$id_proprietario' WHERE id = '$id'";
        if (mysqli_query($conn, $sql)) {
            return true;
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            die;
            return false;
        }
    }
}
