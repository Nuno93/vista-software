<?php
require_once("databaseRepasse.php");

class Repasse extends DatabaseRepasse
{

    private $id_contrato;
    private $data_repasse;
    private $realizado;

    //Metodos Set
    public function setIdContrato($id)
    {
        $this->id_contrato = $id;
    }
    public function setDataRepasse($date)
    {
        $this->data_repasse = $date;
    }
    public function setRealizado($bool)
    {
        $this->realizado = $bool;
    }

    //Metodos Get
    public function getIdContrato()
    {
        return $this->id_contrato;
    }
    public function getDataRepasse()
    {
        return $this->data_repasse;
    }
    public function getRealizado()
    {
        return $this->realizado;
    }

    public function include()
    {
        return $this->setRepasse($this->getIdContrato(), $this->getDataRepasse(), $this->getRealizado());
    }
}
