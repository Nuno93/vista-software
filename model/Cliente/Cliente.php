<?php
require_once("databaseCliente.php");

class Cliente extends DatabaseCliente
{

    private $nome;
    private $email;
    private $telefone;

    //Metodos Set
    public function setNome($string)
    {
        $this->nome = $string;
    }
    public function setEmail($string)
    {
        $this->email = $string;
    }
    public function setTelefone($string)
    {
        $this->telefone = $string;
    }
    //Metodos Get
    public function getNome()
    {
        return $this->nome;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getTelefone()
    {
        return $this->telefone;
    }

    public function include()
    {
        return $this->setCliente($this->getNome(), $this->getEmail(), $this->getTelefone());
    }
}
