<?php

require_once("../../init.php");
class DatabaseCliente
{

    public function setCliente($nome, $email, $telefone)
    {
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        if (!$conn) {
            die("Falha ao conectar no banco de dados: " . mysqli_connect_error());
        }
        $sql = "INSERT INTO clientes (nome, email, telefone) VALUES ('$nome','$email','$telefone')";
        if (mysqli_query($conn, $sql)) {
            return true;
        } else {
            //echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            //die;
            return false;
        }
    }

    public function getCliente()
    {
        $array = [];
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $sql = "SELECT * FROM clientes";
        $clientes = mysqli_query($conn, $sql);
        while ($row = $clientes->fetch_array(MYSQLI_ASSOC)) {
            $array[] = $row;
        }
        return $array;
    }

    public function deleteCliente($id)
    {
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $sql = "DELETE FROM clientes WHERE id = '$id'";
        if (mysqli_query($conn, $sql)) {
            return true;
        } else {
            return false;
        }
    }
    public function searchCliente($id)
    {
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $sql = "SELECT * FROM clientes WHERE id='$id'";
        $cliente = mysqli_query($conn, $sql);
        if (!$cliente) {

            return false;
        } else {

            return $cliente->fetch_array(MYSQLI_ASSOC);
        }
    }
    public function updateCliente($nome, $email, $telefone, $id)
    {
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $sql = "UPDATE clientes SET nome = '$nome', email = '$email', telefone = $telefone WHERE id = '$id'";
        if (mysqli_query($conn, $sql)) {
            return true;
        } else {
            //echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            //die;
            return false;
        }
    }
}
