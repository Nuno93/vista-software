<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

<br />
<nav class="navbar navbar-expand-lg navbar-light style=" background-color:white;"">
    <a class="navbar-brand col-md-7" href="http://www.vistasoft.com.br/">
        <img src="../../view/images/vista.png" width="120" height="30" alt="">
    </a>
    <div class="collapse navbar-collapse  col-md-5" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="/vista_software/index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="/vista_software/view/clientes/index-cliente.php" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Clientes
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/vista_software/view/clientes/index-cliente.php">Listar</a>
                    <a class="dropdown-item" href="/vista_software/view/clientes/store-cliente.php">Cadastrar</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="/vista_software/view/proprietarios/index-proprietario.php" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Proprietários
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/vista_software/view/proprietarios/index-proprietario.php">Listar</a>
                    <a class="dropdown-item" href="/vista_software/view/proprietarios/store-proprietario.php">Cadastrar</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="/vista_software/view/imoveis/index-imovel.php" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Imóveis
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/vista_software/view/imoveis/index-imovel.php">Listar (Banco de Dados)</a>
                    <a class="dropdown-item" href="/vista_software/view/imoveis/store-imovel.php">Cadastrar (Banco de Dados)</a>
                    <a class="dropdown-item" href="/vista_software/view/imoveis/index-imovel-api.php">Listar (API)</a>
                    <a class="dropdown-item" href="/vista_software/view/imoveis/store-imovel-api.php">Cadastrar (API)</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="/vista_software/view/contratos/index-contrato.php" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Contratos
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/vista_software/view/contratos/index-contrato.php">Listar</a>
                    <a class="dropdown-item" href="/vista_software/view/contratos/store-contrato.php">Cadastrar</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
<br />