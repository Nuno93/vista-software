<!DOCTYPE HTML>
<html>
<?php include("../head.php") ?>

<body>
    <div class="container col-md-10">
        <?php include("../menu.php") ?>
        <?php require_once("../../controller/proprietarios/ProprietarioEditController.php"); ?>
        <div class="card card-secondary">
            <div class="card-header">
                <h5 class="card-title">Editar dados de Proprietário</h5>
                <div class="card-tools"></div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <form method="post" action="../../controller/proprietarios/ProprietarioEditController.php" id="form" name="form" onsubmit="validar(document.form); return false;" class="col-12 form-group">
                        <div class="form-group">
                            <div class="form-row">
                                <label class="col-md-2" for="nome">Nome</label>
                                <input class="form-control col-md-10" type="text" id="nome" name="nome" value="<?php echo $edit->getNome(); ?>" required autofocus>
                            </div>
                            <div class="form-row">
                                <label class="col-md-2" for="email">Email</label>
                                <input class="form-control col-md-10" type="text" id="email" name="email" value="<?php echo $edit->getEmail(); ?>" required>
                            </div>
                            <div class="form-row">
                                <label class="col-md-2" for="telefone">Telefone</label>
                                <input class="form-control col-md-10" type="text" id="telefone" name="telefone" value="<?php echo $edit->getTelefone(); ?>" required>
                            </div>
                            <div class="form-row">
                                <label class="col-md-2" for="dia_repasse">Dia do Mês p/ Repasse</label>
                                <input class="form-control col-md-10" type="number" min="1" max="30" id="dia_repasse" name="dia_repasse" value="<?php echo $edit->getDiaRepasse(); ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="id" value="<?php echo $edit->getId(); ?>">
                            <button type="button" class="btn btn-secondary" value='Voltar' onclick='history.go(-1)'>Voltar</button>
                            <button type="submit" class="btn btn-primary" id="edit" name="submit" value="edit">Editar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        function validar(formulario) {
            formulario.submit();
        }
    </script>
</body>

</html>