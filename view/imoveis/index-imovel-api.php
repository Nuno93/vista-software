<?php require_once("../../controller/imoveis/ImovelAPIController.php"); ?>
<!DOCTYPE html>
<html lang="pt-br">

<?php include("../head.php"); ?>

<body>
    <div class="container col-md-10">
        <?php include("../menu.php"); ?>
        <div class="card card-secondary">
            <div class="card-header">
                <h5 class="card-title">Imóveis</h5>
                <div class="card-tools"></div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Cidade</th>
                                <th>Categoria</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php new ImovelAPI();  ?>

                        </tbody>
                    </table>
                    <button type="button" class="btn btn-secondary" value='Voltar' onclick='history.go(-1)'>Voltar</button>
                </div>
            </div>
        </div>
    </div>
</body>

</html>