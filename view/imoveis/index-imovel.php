<?php require_once("../../controller/imoveis/ImovelListController.php"); ?>
<!DOCTYPE html>
<html lang="pt-br">

<?php include("../head.php"); ?>

<body>
    <div class="container col-md-10">
        <?php include("../menu.php"); ?>
        <div class="card card-secondary">
            <div class="card-header">
                <h5 class="card-title">Imóveis</h5>
                <div class="card-tools"></div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Endereco</th>
                                <th>Nome do Proprietário</th>
                                <th>Opções</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php new ImovelList();  ?>

                        </tbody>
                    </table>
                    <a href="store-imovel.php" class="btn btn-primary">Cadastrar</a>
                </div>
            </div>
        </div>
    </div>
</body>

</html>