<!DOCTYPE HTML>
<html>
<?php include("../head.php") ?>

<body>
    <div class="container col-md-10">
        <?php include("../menu.php") ?>
        <div class="card card-secondary">
            <div class="card-header">
                <h5 class="card-title">Cadastrar Imóvel</h5>
                <div class="card-tools"></div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <form method="post" action="../../controller/imoveis/ImovelAPIStoreController.php" id="form" name="form" onsubmit="validate(document.form); return false;" class="col-md-12 form-group">
                        <div class="form-group">
                            <div class="form-row">
                                <label class="col-md-2" for="categoria">Categoria</label>
                                <input class="form-control col-md-10" type="text" id="categoria" name="categoria" placeholder="Categoria do Imóvel" required>
                            </div>
                            <div class="form-row">
                                <label class="col-md-2" for="endereco">Endereço</label>
                                <input class="form-control col-md-10" type="text" id="endereco" name="endereco" placeholder="Endereço do Imóvel" required>
                            </div>
                            <div class="form-row">
                                <label class="col-md-2" for="numeroend">Numero Endereço</label>
                                <input class="form-control col-md-10" type="number" id="numeroend" name="numeroend" placeholder="Número do Endereço" required>
                            </div>
                            <div class="form-row">
                                <label class="col-md-2" for="complemento">Complemento</label>
                                <input class="form-control col-md-10" type="text" id="complemento" name="complemento" placeholder="Complemento do Endereço">
                            </div>
                            <div class="form-row">
                                <label class="col-md-2" for="bairro">Bairro</label>
                                <input class="form-control col-md-10" type="text" id="bairro" name="bairro" placeholder="Bairro do Imóvel" required>
                            </div>
                            <div class="form-row">
                                <label class="col-md-2" for="cidade">Cidade</label>
                                <input class="form-control col-md-10" type="text" id="cidade" name="cidade" placeholder="Cidade do Imóvel" required>
                            </div>
                            <div class="form-row">
                                <label class="col-md-2" for="uf">Estado (UF)</label>
                                <input class="form-control col-md-3" type="text" id="uf" name="uf" placeholder="Estado (UF) do Imóvel" required>
                                <div class="col-md-2"> </div>
                                <label class="col-md-2" for="cep">CEP</label>
                                <input class="form-control col-md-3" type="text" id="cep" name="cep" placeholder="CEP do Imóvel" required>
                            </div>
                            <div class="form-row">
                                <label class="col-md-2" for="situacao">Situação</label>
                                <input class="form-control col-md-3" type="text" id="situacao" name="situacao" placeholder="Situação do Imóvel" required>
                                <div class="col-md-2"> </div>
                                <label class="col-md-2" for="ocupacao">Ocupação</label>
                                <input class="form-control col-md-3" type="text" id="ocupacao" name="ocupacao" placeholder="Ocupação do Imóvel" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" value='Voltar' onclick='history.go(-1)'>Voltar</button>
                            <button type="submit" class="btn btn-primary" id="cadastrar">Cadastrar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        function validate(form) {

            form.submit();
        }
    </script>
</body>

</html>