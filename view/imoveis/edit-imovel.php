<!DOCTYPE HTML>
<html>
<?php include("../head.php") ?>

<body>
    <div class="container col-md-10">
        <?php include("../menu.php") ?>
        <?php require_once("../../controller/imoveis/ImovelEditController.php"); ?>
        <div class="card card-secondary">
            <div class="card-header">
                <h5 class="card-title">Editar dados de Imóvel</h5>
                <div class="card-tools"></div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <form method="post" action="../../controller/imoveis/ImovelEditController.php" id="form" name="form" onsubmit="validar(document.form); return false;" class="col-12 form-group">
                        <div class="form-group">
                            <div class="form-row">
                                <label class="col-md-2" for="endereco">Endereco</label>
                                <input class="form-control col-md-10" type="text" id="endereco" name="endereco" value="<?php echo $edit->getEndereco(); ?>" required autofocus>
                            </div>
                            <div class="form-row">
                                <label class="col-md-2" for="proprietario">Proprietário</label>
                                <input class="form-control col-md-10" type="text" id="id_proprietario" name="id_proprietario" value="<?php echo $edit->getIdProprietario(); ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="id" value="<?php echo $edit->getId(); ?>">
                            <button type="button" class="btn btn-secondary" value='Voltar' onclick='history.go(-1)'>Voltar</button>
                            <button type="submit" class="btn btn-primary" id="edit" name="submit" value="edit">Editar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        function validar(formulario) {
            formulario.submit();
        }
    </script>
</body>

</html>