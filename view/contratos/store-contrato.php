<?php require_once("../../controller/imoveis/ImovelSelectController.php"); ?>
<?php require_once("../../controller/clientes/ClienteSelectController.php"); ?>
<!DOCTYPE HTML>
<html>
<?php include("../head.php") ?>

<body>
    <div class="container col-md-10">
        <?php include("../menu.php") ?>
        <div class="card card-secondary">
            <div class="card-header">
                <h5 class="card-title">Cadastrar Contrato</h5>
                <div class="card-tools"></div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <form method="post" action="../../controller/contratos/ContratoStoreController.php" id="form" name="form" onsubmit="validate(document.form); return false;" class="col-md-12 form-group">
                        <div class="form-group">
                            <div class="form-row">
                                <label class="col-md-2" for="id_cliente">Cliente</label>
                                <select class="form-control col-md-10" name="id_cliente" id="id_cliente">
                                    <option value="0">Selecione...</option>
                                    <?php new ClienteSelect();  ?>
                                </select>
                            </div>
                            <div class="form-row">
                                <label class="col-md-2" for="id_imovel">Imóvel</label>
                                <select class="form-control col-md-10" name="id_imovel" id="id_imovel">
                                    <option value="0">Selecione...</option>
                                    <?php new ImovelSelect();  ?>
                                </select>
                            </div>
                            <div class="form-row">
                                <label class="col-md-2" for="data_inicio">Data de Início do Contrato</label>
                                <input class="form-control col-md-3" type="date" id="data_inicio" name="data_inicio" placeholder="Data Inicial" required>
                                <div class="col-md-2"> </div>
                                <label class="col-md-2" for="data_inicio">Data de Fim do Contrato</label>
                                <input class="form-control col-md-3" type="date" id="data_fim" name="data_fim" placeholder="Data Final" required>
                            </div>
                            <div class="form-row">
                                <label class="col-md-2" for="taxa_adm">Taxa de Administração</label>
                                <input class="form-control col-md-3" type="text" id="taxa_adm" name="taxa_adm" placeholder="Taxa Adm" pattern="^\d*(\.\d{0,2})?$" required>
                                <div class="col-md-2"> </div>
                                <label class="col-md-2" for="valor_aluguel">Valor do Aluguel</label>
                                <input class="form-control col-md-3" type="text" id="valor_aluguel" name="valor_aluguel" placeholder="Valor Aluguel" pattern="^\d*(\.\d{0,2})?$" required>
                            </div>
                            <div class="form-row">
                                <label class="col-md-2" for="valor_condominio">Valor do Condominio</label>
                                <input class="form-control col-md-3" type="text" id="valor_condominio" name="valor_condominio" placeholder="Valor Condominio" pattern="^\d*(\.\d{0,2})?$" required>
                                <div class="col-md-2"> </div>
                                <label class="col-md-2" for="valor_iptu">Valor do IPTU</label>
                                <input class="form-control col-md-3" type="text" id="valor_iptu" name="valor_iptu" placeholder="Valor IPTU" pattern="^\d*(\.\d{0,2})?$" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" value='Voltar' onclick='history.go(-1)'>Voltar</button>
                            <button type="submit" class="btn btn-primary" id="cadastrar">Cadastrar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        function validate() {
            var data_inicial = form.data_inicial.value;
            var data_final = form.data_final.value;
            alert(data_inicial);
            return false;
            if (data_inicial > data_final) {
                alert('A data inicial não pode ser maior que a data final!');
                form.data_inicial.focus();
                return false;
            }
            if (data_inicial < new Date) {
                alert('A data inicial precisa ser igual ou superior ao dia de hoje!');
                form.data_inicial.focus();
                return false;
            }
        }

        $(document).on('keydown', 'input[pattern]', function(e) {
            var input = $(this);
            var oldVal = input.val();
            var regex = new RegExp(input.attr('pattern'), 'g');

            setTimeout(function() {
                var newVal = input.val();
                if (!regex.test(newVal)) {
                    input.val(oldVal);
                }
            }, 0);
        });
    </script>
</body>

</html>