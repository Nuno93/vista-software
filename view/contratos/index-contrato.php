<?php require_once("../../controller/contratos/ContratoListController.php"); ?>
<!DOCTYPE html>
<html lang="pt-br">

<?php include("../head.php"); ?>

<body>
    <div class="container col-md-10">
        <?php include("../menu.php"); ?>
        <div class="card card-secondary">
            <div class="card-header">
                <h5 class="card-title">Contratos</h5>
                <div class="card-tools"></div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Nome do Cliente</th>
                                <th>Nome do Proprietário</th>
                                <th>Valor Aluguel </th>
                                <th>Data Inicio </th>
                                <th>Data Fim </th>
                                <th>Mensal.</th>
                                <th>Repasses</th>
                                <th>Opções</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php new ContratoList();  ?>

                        </tbody>
                    </table>
                    <a href="store-contrato.php" class="btn btn-primary">Cadastrar</a>
                </div>
            </div>
        </div>
    </div>
</body>

</html>