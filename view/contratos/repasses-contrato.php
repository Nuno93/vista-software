<?php require_once("../../controller/repasses/RepasseListController.php"); ?>
<!DOCTYPE html>
<html lang="pt-br">

<?php include("../head.php"); ?>

<body>
    <div class="container col-md-10">
        <?php include("../menu.php"); ?>
        <div class="card card-secondary">
            <div class="card-header">
                <h5 class="card-title">Repasses do Contrato</h5>
                <div class="card-tools"></div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Número do contrato</th>
                                <th>Data do Repasse</th>
                                <th>Realização do Repasse</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php new RepasseList($_GET['id']);  ?>

                        </tbody>
                    </table>
                    <button type="button" class="btn btn-secondary" value='Voltar' onclick='history.go(-1)'>Voltar</button>
                </div>
            </div>
        </div>
    </div>
</body>

</html>