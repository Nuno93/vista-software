<?php require_once("../../controller/mensalidades/MensalidadeListController.php"); ?>
<!DOCTYPE html>
<html lang="pt-br">

<?php include("../head.php"); ?>

<body>
    <div class="container col-md-10">
        <?php include("../menu.php"); ?>
        <div class="card card-secondary">
            <div class="card-header">
                <h5 class="card-title">Mensalidades do Contrato</h5>
                <div class="card-tools"></div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Número do Contrato</th>
                                <th>Valor da Mensalidade</th>
                                <th>Data de Vencimento Mensalidade </th>
                                <th>Pagamento de Mensalidade</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php new MensalidadeList($_GET['id']);  ?>

                        </tbody>
                    </table>
                    <button type="button" class="btn btn-secondary" value='Voltar' onclick='history.go(-1)'>Voltar</button>
                </div>
            </div>
        </div>
    </div>
</body>

</html>