<!DOCTYPE HTML>
<html>
<?php include("../head.php") ?>

<body>
    <div class="container col-md-10">
        <?php include("../menu.php") ?>
        <div class="card card-secondary">
            <div class="card-header">
                <h5 class="card-title">Cadastrar Cliente</h5>
                <div class="card-tools"></div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <form method="post" action="../../controller/clientes/ClienteStoreController.php" id="form" name="form" onsubmit="validate(document.form); return false;" class="col-12 form-group">
                        <div class="form-group">
                            <div class="form-row">
                                <label class="col-md-2" for="nome">Nome</label>
                                <input class="form-control col-md-10" type="text" id="nome" name="nome" placeholder="Nome Completo" required>
                            </div>
                            <div class="form-row">
                                <label class="col-md-2" for="email">E-mail</label>
                                <input class="form-control col-md-10" type="text" id="email" name="email" placeholder="E-mail" required>
                            </div>
                            <div class="form-row">
                                <label class="col-md-2" for="telefone">Telefone</label>
                                <input class="form-control col-md-10" type="text" id="telefone" name="telefone" placeholder="Telefone" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" value='Voltar' onclick='history.go(-1)'>Voltar</button>
                            <button type="submit" class="btn btn-primary" id="cadastrar">Cadastrar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        function validate(form) {

            form.submit();
        }
    </script>
</body>

</html>